package com.example.tp2;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.DropBoxManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class BookActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        Intent intent = getIntent();
        final Book book = intent.getParcelableExtra("book");
        final int requestID = intent.getIntExtra("request", 0);

        final TextView nameBook = findViewById(R.id.nameBook);
        nameBook.setText(book.getTitle());

        final TextView editAuthors = findViewById(R.id.editAuthors);
        editAuthors.setText(book.getAuthors());

        final TextView editYear = findViewById(R.id.editYear);
        editYear.setText(book.getYear());

        final TextView editGenres = findViewById(R.id.editGenres);
        editGenres.setText(book.getGenres());

        final TextView editPublisher = findViewById(R.id.editPublisher);
        editPublisher.setText(book.getPublisher());

        Button saveButton = findViewById(R.id.button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(String.valueOf(nameBook.getText()).equals("") == true ) {
                    AlertDialog alertDialog = new AlertDialog.Builder(BookActivity.this).create();
                    alertDialog.setTitle("Sauvegarde impossible");
                    alertDialog.setMessage("Le titre du livre doit être non vide !");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "CANCEL", new DialogInterface.OnClickListener() { public void onClick(DialogInterface dialog, int which) { dialog.dismiss(); }});
                    alertDialog.show();
                }else {
                    book.setTitle(nameBook.getText().toString());
                    book.setAuthors(editAuthors.getText().toString());
                    book.setYear(editYear.getText().toString());
                    book.setGenres(editGenres.getText().toString());
                    book.setPublisher(editPublisher.getText().toString());
                    BookDbHelper BDbHelper = new BookDbHelper(BookActivity.this);
                    switch (requestID) {
                        case 1:
                            BDbHelper.updateBook(book);
                            Toast.makeText(BookActivity.this, "Le livre '" + book.getTitle() + "' à été mis à jour", Toast.LENGTH_LONG).show();
                            break;
                        case 2:
                            //BDbHelper.duplicate(book);
                            BDbHelper.addBook(book);
                            Toast.makeText(BookActivity.this, "Le livre '" + book.getTitle() + "' à été ajouter", Toast.LENGTH_LONG).show();
                            break;
                    }
                    finish();
                }
            }
        });
    }

}
