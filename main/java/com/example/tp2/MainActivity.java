package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.util.ArrayList;

import static com.example.tp2.BookDbHelper.cursorToBook;


public class MainActivity extends AppCompatActivity {

    private SimpleCursorAdapter SCAdapter;
    private BookDbHelper BDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ListView booksListView = findViewById(R.id.bookList);

        BDbHelper = new BookDbHelper(this);
        BDbHelper.populate();

        SCAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                BDbHelper.fetchAllBooks(),
                new String[]{BookDbHelper.COLUMN_BOOK_TITLE, BookDbHelper.COLUMN_AUTHORS},
                new int[]{android.R.id.text1, android.R.id.text2}, 0);
        booksListView.setAdapter(SCAdapter);

        booksListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, BookActivity.class);
                intent.putExtra("book", cursorToBook((Cursor) parent.getItemAtPosition(position)));
                intent.putExtra("request", 1);
                startActivityForResult(intent, 1);
            }
        });

        final FloatingActionButton addActionButton = findViewById(R.id.addBookActionButton);
        addActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book newBook = new Book(null, null, null ,null, null);
                Intent intent = new Intent(MainActivity.this, BookActivity.class);
                intent.putExtra("book", newBook);
                intent.putExtra("request", 2);
                startActivityForResult(intent, 2);
            }
        });

        booksListView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                AdapterView.AdapterContextMenuInfo del = (AdapterView.AdapterContextMenuInfo) menuInfo;
                menu.add(0, 0, 0, "Supprimer");
            }
        });

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        AdapterView adapter = findViewById(R.id.bookList);
        Cursor target = (Cursor) adapter.getItemAtPosition(info.position);
        BookDbHelper dBook = new BookDbHelper(MainActivity.this);
        dBook.deleteBook(target);
        Intent delIntent = new Intent(MainActivity.this,MainActivity.class);
        startActivity(delIntent);
        Toast.makeText(MainActivity.this, "Le livre '" + cursorToBook(target).getTitle()  + "' à été supprimer", Toast.LENGTH_LONG).show();
        finish();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        SCAdapter.changeCursor(BDbHelper.fetchAllBooks());
        SCAdapter.notifyDataSetChanged();
    }
}
