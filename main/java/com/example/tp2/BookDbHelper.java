package com.example.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class BookDbHelper extends SQLiteOpenHelper {

    private static final String TAG = BookDbHelper.class.getSimpleName();

    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "book.db";
    public static final String TABLE_NAME = "library";
    public static final String _ID = "_id";
    public static final String COLUMN_BOOK_TITLE = "title";
    public static final String COLUMN_AUTHORS = "authors";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_GENRES = "genres";
    public static final String COLUMN_PUBLISHER = "publisher";

    public BookDbHelper(Context context) { super(context, DATABASE_NAME, null, DATABASE_VERSION); }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(String.format("CREATE TABLE IF NOT EXISTS %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT )",
                TABLE_NAME,
                _ID,
                COLUMN_BOOK_TITLE,
                COLUMN_AUTHORS,
                COLUMN_YEAR,
                COLUMN_GENRES,
                COLUMN_PUBLISHER));
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    /**
     * Adds a new book
     * @return  true if the book was added to the table ; false otherwise
     */
    public boolean addBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(COLUMN_BOOK_TITLE, book.getTitle());
        cv.put(COLUMN_AUTHORS, book.getAuthors());
        cv.put(COLUMN_YEAR, book.getYear());
        cv.put(COLUMN_GENRES, book.getGenres());
        cv.put(COLUMN_PUBLISHER, book.getPublisher());

        long rowID = db.insert(TABLE_NAME, null, cv);

        db.close();

        return (rowID != -1);
    }


    /**
     * Updates the information of a book inside the data base
     * @return the number of updated rows
     */
    public int updateBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(COLUMN_BOOK_TITLE, book.getTitle());
        cv.put(COLUMN_AUTHORS, book.getAuthors());
        cv.put(COLUMN_YEAR, book.getYear());
        cv.put(COLUMN_GENRES, book.getGenres());
        cv.put(COLUMN_PUBLISHER, book.getPublisher());

        int res = db.update(TABLE_NAME, cv,"_id = "+ Long.toString(book.getId()), null);

        db.close();

        if(res != 0) { return res; }

        return 0;
    }

    public Cursor fetchAllBooks() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);

        if (cursor != null) { cursor.moveToFirst(); }
        return cursor;
    }

    public void deleteBook(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME,_ID + "=?", new String[] { Long.toString(cursorToBook(cursor).getId()) });
        db.close();
    }

    public void populate() {
        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM " + TABLE_NAME, null);
        if (numRows > 0) { db.close(); return; }
        db.close();

        Log.d(TAG, "call populate()");
        addBook(new Book("Rouge Brésil", "J.-C. Rufin", "2003", "roman d'aventure, roman historique", "Gallimard"));
        addBook(new Book("Guerre et paix", "L. Tolstoï", "1865-1869", "roman historique", "Gallimard"));
        addBook(new Book("Fondation", "I. Asimov", "1957", "roman de science-fiction", "Hachette"));
        addBook(new Book("Du côté de chez Swan", "M. Proust", "1913", "roman", "Gallimard"));
        addBook(new Book("Le Comte de Monte-Cristo", "A. Dumas", "1844-1846", "roman-feuilleton", "Flammarion"));
        addBook(new Book("L'Iliade", "Homère", "8e siècle av. J.-C.", "roman classique", "L'École des loisirs"));
        addBook(new Book("Histoire de Babar, le petit éléphant", "J. de Brunhoff", "1931", "livre pour enfant", "Éditions du Jardin des modes"));
        addBook(new Book("Le Grand Pouvoir du Chninkel", "J. Van Hamme et G. Rosiński", "1988", "BD fantasy", "Casterman"));
        addBook(new Book("Astérix chez les Bretons", "R. Goscinny et A. Uderzo", "1967", "BD aventure", "Hachette"));
        addBook(new Book("Monster", "N. Urasawa", "1994-2001", "manga policier", "Kana Eds"));
        addBook(new Book("V pour Vendetta", "A. Moore et D. Lloyd", "1982-1990", "comics", "Hachette"));

        db = this.getReadableDatabase();
        numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM " + TABLE_NAME, null);
        Log.d(TAG, "nb of rows=" + numRows);
        db.close();
    }

    public static Book cursorToBook(Cursor cursor) {
        Book book = new Book();
        book.setId(Long.parseLong(cursor.getString(cursor.getColumnIndex(_ID))));
        book.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_BOOK_TITLE)));
        book.setAuthors(cursor.getString(cursor.getColumnIndex(COLUMN_AUTHORS)));
        book.setYear(cursor.getString(cursor.getColumnIndex(COLUMN_YEAR)));
        book.setGenres(cursor.getString(cursor.getColumnIndex(COLUMN_GENRES)));
        book.setPublisher(cursor.getString(cursor.getColumnIndex(COLUMN_PUBLISHER)));
        return book;
    }

    /*
    public void duplicate(Book book){
        SQLiteDatabase db = this.getReadableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(COLUMN_BOOK_TITLE, book.getTitle());
        cv.put(COLUMN_AUTHORS, book.getAuthors());
        cv.put(COLUMN_YEAR, book.getYear());
        cv.put(COLUMN_GENRES, book.getGenres());
        cv.put(COLUMN_PUBLISHER, book.getPublisher());


        long rowID = db.insertWithOnConflict(TABLE_NAME, null, cv, SQLiteDatabase.CONFLICT_REPLACE);

        Cursor tmp = db.query(String.format("SELECT * FROM %s WHERE %s = %s OR %s = %s", TABLE_NAME, COLUMN_BOOK_TITLE, book.getTitle(), COLUMN_AUTHORS, book.getAuthors() ));

        Log.d("req", Long.toString(rowID));
    }*/
}
